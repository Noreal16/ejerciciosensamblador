;Ejercicio tabla de multiplicar 
;Alex Santiago Nole Reyes
;6 A
;2020

%macro escritura 2
        mov eax, 4
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro

section .data
        multi db ' * ',10
        resultado db ' = ',10
        new_line db 10,''
section .bss
        multipliccion resb 2
        item resb 2
section .text
        global _start
_start:
        mov ax, 3
        mov rcx, 9

mensajes:

for:
        push rcx
        mov rbx, rcx
        
        mul bx
        add ax, '0'
        
        mov [multipliccion],ax
        
        add ebx, '0'
        mov [item], ebx

        escritura item, 2
        escritura multi, 1
        
        mov eax, 4
        mov ebx, 1
        mov ecx, multipliccion
        mov edx, 2
        int 80h

        mov eax, 4
        mov ebx, 1
        mov ecx, new_line
        mov edx, 1
        int 80h
        
        mov eax, 4
        mov ebx, 1
        mov ecx, resultado
        mov edx, 1
        int 80h

        pop rcx
        loop for
salir:
        mov eax, 1
        int 80h