;Leer 2 numero por teclado y restar
section .data
    valorA db 'Ingrese primer valor', 10
    len_valorA equ $-valorA

    valorB db 'Ingrese segundo valor', 10
    len_valorB equ $-valorB

    resultado db 'El resultado de resta es', 10
    len_resultado equ $-resultado

    new_line db " ",10
    len_new_line equ $-new_line

section .bss
 numeroA resb 5
 numeroB resb 5
 result resb 5

section .text
global _start
_start:
;********************Imprime Mensaje*************************
 mov eax, 4
 mov ebx, 1
 mov ecx, valorA
 mov edx, len_valorA
 int 80h
;*******************Lee valores***************************
mov eax, 3
mov ebx, 2
mov ecx, numeroA
mov edx, 5
int 80h
;********************Imprime Mensaje*************************
 mov eax, 4
 mov ebx, 1
 mov ecx, valorB
 mov edx, len_valorB
 int 80h
;*******************Lee valores***************************
mov eax, 3
mov ebx, 2
mov ecx, numeroB
mov edx, 5
int 80h
;*******************Operacion****************************
mov eax, [numeroA]
sub eax, '0'
mov ebx, [numeroB]
sub ebx, '0'

sub eax, ebx
add eax, '0'

mov [result], eax
;******************Imprime el valor de la resta***********
mov eax, 4
mov ebx, 1
mov ecx, resultado
mov edx, len_resultado
int 80h

mov eax, 4
mov ebx, 1
mov ecx, result
mov edx, 5
int 80h

mov eax, 4
mov ebx, 1
mov ecx, new_line
mov edx, len_new_line
int 80h

;**********************Salida *************************
mov eax, 1
int 80h
