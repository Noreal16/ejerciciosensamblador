;Suma de dos numero estaticos
section .data
   resultado db 'El resultado es:',10
   len_resultado equ $-resultado


section .bss
    suma resb 1 ;cuando no se nececita del enter
section .text
    global _start
_start:
    mov eax,6
    mov ebx,2
    add eax, ebx ;eax=eax+ebx
    add eax, '0' ;Ajuste para que haga la suma en ascii

    mov [suma], eax

    mov eax, 4
    mov ebx, 1
    mov ecx, resultado
    mov edx, len_resultado
    int 80h

    mov eax, 4
    mov ebx, 1
    mov ecx, suma
    mov edx, 1
    int 80h

    mov eax, 1
    int 80h
