%macro escritura 2
        mov eax, 4
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro
section	.data
	msj db '*', 10
	new_line db 10,''

section	.text
   global _start        ;must be declared for using gcc
	
_start:	
    mov rcx, 8
    mov rbx, 1

l1:
    push rcx
    push rbx
    mov rcx, rbx
l2:
    ;*************************nueva linea*******************
    mov eax, 4
    mov ebx, 1
    mov ecx, new_line
    mov edx, 1
    int 80h
    ;**********************imprimir asterisco**************
    mov eax, 4
    mov ebx, 1
    mov ecx, msj
    mov edx, 1
    int 80h

    pop rcx
    loop l2 ;salto a l2, dec cx
    ;********************finaliza primer loop columnas**************
    pop rbx
    pop rcx
    inc rbx
    loop l1
    ;********************finaliza primer loop filas**************
salir:
    mov eax, 1
    int 80h