;Alex Santiago Nole Reyes 
;22 de julio de 2020
;Cuadrado de asteriscos

;programa que imprima ***********
section .data
        asterisco db '*'
        new_line db 10,' ' 
section .text
 	global _start
 
_start:
        mov ax, 9 
        mov bx, 9
        jmp imprimir
imprimir:

        push ax  ;con el dec va decrementando en valor en pila 1
        push bx

        mov eax, 4
        mov ebx, 1
        mov ecx, asterisco
        mov edx, 1
        int 80h
        
        pop bx
        pop ax
        dec ax
        
        cmp ax, 0
        jz imprimir2
        jmp imprimir ; cuando no sea 0 imprime 

imprimir2:
        dec  bx
        push bx
        
        mov eax, 4
        mov ebx, 1
        mov ecx, new_line
        mov edx, 1
        int 80h

        
        pop bx
        mov ax, 9 
       
        cmp bx, 0
        
        jz salir
        jmp imprimir
        
salir: 
        mov eax,1
	int 80h