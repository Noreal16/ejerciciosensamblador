;----------------------------------------------------------------------------------------------------------
;Tarea
;Estudiante: ALex Santiago Nole Reyes
;Ciclo: 6 "A"
;----------------------------------------------------------------------------------------------------------
;sintaxis de macros 
;%macro macro_name  number_of_params
;<macro body>
;%endmacro
;definición de una macro %macro macro_name number_of_parameters
;----------------------------Inicio del macro imprimit----------------------------------------------------
%macro imprimir 2 
	mov eax,4  	;Tipo de subrutina operacion de escritura system _writer
	mov ebx,1  	;estandar de tipo de salida 
	mov ecx,%1 	;etiqueta permite obtener la primera entrada de escritura(mensaje que va a escribir)
	mov edx,%2 	;cantidad de digitos a imprimir(tamaño del mensaje)
	int 80H		; interrupcion
%endmacro	
;Fin del macro imprimir	
;-------------------------------Inicio del macro leer------------------------------------------------------ 
%macro leer 2
    mov eax,3	;Tipo de subrutina operacion para lectura system _read
    mov ebx,0	;estandar de tipo de lectura desde la entrada
    mov ecx,%1 	;etiqueta permite obtener la primera entrada de lectura(mensaje que va a leer)
    mov edx,%2	;Cantidad de digitos a leer
    int 80H 	;interrupcion
%endmacro
;FIn del macro leer

; ecx,modo de acceso
; edx, permisos
section .bss			;seccion de codigo
	auxiliar resb 30  	;efine un arreglo (auxiliar) de 30 bytes sin valor inicial
	auxiliarb resb 30	;efine un arreglo (auxiliarb) de 30 bytes sin valor inicial
	auxiliarc resb 30	;efine un arreglo (auxiliarc) de 30 bytes sin valor inicial


section .data				;seccion de datos
	msg db 0x1b ,"       " 	; 6 espacios para contener al dato
	lenmsg equ $-msg		; define el numero de caracteres de la constante msg



	salto db " ",10 		; 1 espacio para realizar un salto
	lensalto equ $-salto	;define el numero de especios que tiene salto




section .text			
    global _start    	; define el inicio del segmento

_start:
	
	mov ecx,9			;es el numero contador del bucle 

	mov al,0			;AL contendra el numero de la operacion 
	mov [auxiliar],al 	;copia  el contenido de al a la direccion de memoria AUXILIAR

cicloI:				;Utilizacion de una etiqueta inicia el cicloJ
	push ecx		;guarda el valor de ecx en la pila
	mov ecx,9		;es el numero contador del ciclo

	mov al,0				;AL contendra el numero de operacion
	mov [auxiliarb],al 		;copia  el contenido de al a la direccion de memoria AUXILIARB

	cicloJ:			;Utilizacion de una etiqueta inicia el cicloJ
		push ecx	;Guarda el valor de ecx en la pila


		call imprimir0al9	;llama a las subrutinas
							;	imprimir msg2,lenmsg2

	fincicloJ:					;Fin ciclo J
		mov al,[auxiliarb]   	;se realiza un desplazamiento
		inc al					; incrementa AL en 1
		mov [auxiliarb],al 		;copia  el contenido de al a la direccion de memoria AUXILIARB

		pop ecx					;restaura la direccion de ecx
		loop cicloJ				;decrementar ECX  iterar si es diferente de 0
		
	;imprimir salto,lensalto

fincicloI:						;Fin ciclo I
	mov al,[auxiliar]			;se realiza un desplazamiento
	inc al						; incrementa AL en 1
	mov [auxiliar],al			;copia  el contenido de al a la direccion de memoria AUXILIAR

	pop ecx						;restaura la direccion de ecx
	loop cicloI					;decrementar ECX  iterar si es diferente de 0
	

salir:							;Finaliza el proceso
	mov eax, 1					;salida del oprograma, system _exit
	int 80H						;Interrupcion del sistema 



imprimir0al9:
	
	mov ebx,"["				; le asiga el valor de la cadena "[" al valor de ebx
	mov [msg+1], ebx		; copiar ebx en [msg+1] en la posicion 1(Direccionamiento con desplazamiento directo)

	mov bl,[auxiliar]		;Copia el valor de la direccion del auxiliar en bl 
	add bl,'0'				; se agrega el valor de la cadena del codigo ascii a bl
	mov [msg+2], bl			; copiar bl en [msg+2] en la posicion 2 (Direccionamiento con desplazamiento directo)


	mov ebx,";"				;le asigna le valor de ";" al registro ebx
	mov [msg+3], ebx		;copiar ebx en [msg+3] en la posicion 3 (Direccionamiento con desplazamiento directo)

	
	mov bl,[auxiliarb]		;Copia el valor de la direccion del auxiliarB en bl 
	add bl,'0'				;se agrega el valor de la cadena del codigo ascii a bl
	mov [msg+4],bl			;copiar bl en [msg+4] en la poscicion 4(Direccionamiento con desplazamiento directo)

	mov ebx,"fJ"			;le asigna le valor de "fj" al registro ebx
	mov [msg+5], ebx		;copiar ebx en [msg+5] en la posicion 5(Direccionamiento con desplazamiento directo)

	imprimir msg,lenmsg		;llama al macro y presenta el valor msg y lenmsg 

	ret						;Retorno 
