;Operaciones  de dos numero estaticos
;macros se los utiliza para entradas y salidas
%macro imprimir 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro 

section .data
    resultado_suma db 'El resultado de la suma es', 10
    len_resultado_suma  equ $-resultado_suma

   resultado_resta db 'El resultado de la resta es:',10
   len_resultado_resta equ $-resultado_resta

   resultado_multiplicacion db 'El resultado de la multiplicacion es:',10
   len_resultado_mul equ $-resultado_multiplicacion

   resultado_cociente db 'El resultado de la division es:',10
   len_resultado_cociente equ $-resultado_cociente
   
   resultado_residuo db 'El resultado de la division es:',10
   len_resultado_residuo equ $-resultado_residuo
    
    new_line db " ",10
    len_new_line equ $-new_line

section .bss
    suma resb 1 ;cuando no se nececita del enter
    resta resb 1
    multiplicacion resb 1
    cociente resb 1
    residuo resb 1
section .text
    global _start
_start:
    mov eax,4
    mov ebx,2
    add eax, ebx ;eax=eax+ebx
    add eax, '0' ;Ajuste para que haga la suma en ascii
    mov [suma], eax
    
    imprimir resultado_suma, len_resultado_suma
    imprimir suma, 1

    imprimir new_line, len_new_line

    
    ;**********************************************************
    mov eax,4
    mov ebx,2
    sub eax, ebx ;eax=eax-ebx
    add eax, '0' 
    mov [resta], eax
    
    imprimir resultado_resta, len_resultado_resta
    imprimir resta, 1
    
    imprimir new_line, len_new_line
    ;*******************************************************************************
    mov eax,4
    mov ebx,2
    mul ebx ;eax=eax*ebx 
    add eax, '0'
    mov [multiplicacion], eax
    
    imprimir resultado_multiplicacion, len_resultado_mul
    imprimir multiplicacion, 1

    imprimir new_line, len_new_line
    ;***************************Division**************************************************
    mov al,4
    mov bl,2
    div bl ;eax=eax/ebx
    add al, '0'
    mov [cociente], al
    add ah, '0'
    mov [residuo], ah

    imprimir resultado_cociente, len_resultado_cociente
    imprimir cociente, 1

    imprimir new_line, len_new_line

    imprimir resultado_residuo, len_resultado_residuo
    imprimir residuo, 1

    imprimir new_line, len_new_line
    ;******************Salida*************************
    mov eax, 1
    int 80h
