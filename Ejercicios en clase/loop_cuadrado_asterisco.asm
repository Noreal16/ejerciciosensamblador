;Ejercicio loop cuadrado asterisco
;Alex Santiago Nole Reyes
;6 A
;20-julio-2020

%macro escritura 2
        mov eax, 4
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro

section .data
        asterisco db '*'
        new_line db 10,''

section .text
        global _start
_start:
        mov rbx, 9 ;filas
        mov rcx, 9 ;columnas
inicio:
    push rbx
    cmp  rbx, 0
    jz salir
    jmp ciclo1
ciclo1:
        push rcx
        escritura asterisco, 1 ;el valor de cx se reemplaza con asterisco '*'
        pop rcx
        loop ciclo1
ciclo2:
        escritura new_line, 1
        pop rbx
        dec rbx
        mov rcx, 9
        jmp inicio

salir:
        mov eax, 1
        int 80h