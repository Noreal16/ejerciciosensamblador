section .data
    mensaje db 'ingrese un numero', 10
    len_mensaje equ $-mensaje
    menjase_presentacion db 'El numero ingresado', 10
    len_presentado equ $-menjase_presentacion
section .bss
    numero resb 5 ; reserva 5 espacion caracteres

section .text
    global _start
_start:
    ;***************************Imprime mensaje**************
    mov eax, 4
    mov ebx, 1
    mov ecx, mensaje
    mov edx, len_mensaje
    int 80h
    ;----------------------Lectura mensaje-------------------------
    mov eax, 3          ; define el tipo de operacion system.read
    mov ebx, 2          ;define el tipo de estandar de entrada
    mov ecx, numero     ;lo que captura en teclado
    mov edx, 5          ;tiene que coincidir con e valor del bss(numero de caracteres)
    int 80h             ;interrupcion del sistema
    ;**************************Mensaje presentacion********************
    mov eax, 4
    mov ebx, 1
    mov ecx, menjase_presentacion
    mov edx, len_presentado
    int 80h
    ;**************************Imprime numero********************
    mov eax, 4
    mov ebx, 1
    mov ecx, numero
    mov edx, 5
    int 80h
    ;********************Salir del sistema*******************
    mov eax, 1
    int 80h