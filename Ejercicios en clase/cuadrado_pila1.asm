section .data

    asterisco db "*"
    espacio db 10,""

section .text

        global _start

_start:

    mov cx, 9
    mov dx, 9

primerNivel:

	cmp cx,0
	jg imprimir
	je segundoNivel

imprimir:
	push cx
	push dx
    mov eax, 4
    mov ebx, 1
    mov ecx, asterisco
    mov edx, 1
    int 80h
	pop dx
	pop cx
	dec cx
	jmp primerNivel
	
segundoNivel:

	dec dx
	push dx
    mov eax, 4
    mov ebx, 1
    mov ecx, espacio
    mov edx, 1
    int 80h
	pop dx
	mov cx,9
	
	cmp dx,0
	jg primerNivel
	je salir


salir: 
	mov eax,1
	int 80h