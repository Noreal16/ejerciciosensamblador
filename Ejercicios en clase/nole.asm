%macro imprimir 2 
        mov eax, 4
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro

%macro leer 2 
        mov eax, 3
        mov ebx, 2
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro

section .data
        msj db  "Ingrese un Número:",10
        len_msj equ $ - msj

        msj1 db "Ingrese un Número:",10
        len_msj1 equ $ - msj1

        msj2 db "El cociente es : "
        len_msj2 equ $ - msj2

        msj3 db "El residuo es : "
        len_msj3 equ $ - msj3

        new_line db "",10
        len_new_line equ $ - new_line

section .bss
        valor1 resb 1
        valor2 resb 2
        resta resb 1
        cociente resb 2
        residuo resb 2
section .text
        global _start ; establece una posición de memoria
_start:
        ;****************************Leer primer valor**************************************
        imprimir msj, len_msj 
        leer valor1, 2
        ;****************************Leer segundo valor**************************************
        imprimir msj1, len_msj1 
        leer valor2, 2
principal:
        mov ah, [valor1] ; ah para obtener el residuo 
        sub ah, '0'
	    mov bl, [valor2]
        sub bl, '0' 
	    mov cl, 0
        cmp cl, 0
        je ciclo
ciclo: 
        sub ah, bl
	    inc cl
	    cmp ah, bl
	    jge ciclo
	    add cl,'0'
	    mov [cociente], cl
        add ah,'0'
	    mov [residuo], ah
        ;*******************************imprimir resultados **************************************
        imprimir msj2, len_msj2
        imprimir cociente, 2
        imprimir new_line, len_new_line
        imprimir msj3, len_msj3
        imprimir  residuo, 2
        imprimir new_line, len_new_line
salir: 
        mov eax, 1
        int 80h
