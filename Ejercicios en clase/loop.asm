;loop 

section .data
        saludo db  10,  'hola'
        len_saludo equ $-saludo

        espacio db ':'

        len_espacio equ $-espacio
section .bss
        item resb 2
section .text
        global _start
_start:
       mov rcx, 10

for:
   push rcx

   mov eax, ecx
   add eax, '0'
   mov [item], eax

   mov eax, 4
   mov ebx, 1
   mov ecx, item
   mov edx, 2
   int 80h

   mov eax, 4
   mov ebx, 1
   mov ecx, espacio
   mov edx, len_espacio
   int 80h
   
   mov eax, 4
   mov ebx, 1
   mov ecx, saludo
   mov edx, len_saludo
   int 80h
   pop rcx
   loop for
salir:
        mov eax, 1
        int 80h