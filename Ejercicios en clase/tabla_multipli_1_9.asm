; Omar Sanmartin
; Fecha 3 de Agosto de 2020
; Deber Tabla de Multiplicar Multisim
%macro imprimir 2
    mov eax,4
    mov ebx,1
    mov ecx,%1
    mov edx,%2
    int 80h
%endmacro
section .data
    msj1 db ' * '
    len_msj1 equ $-msj1
    
    msj2 db ' = '
    len_msj2 equ $-msj2
    
    salto db 10
    len_salto equ $-salto

    separador db '****************************',10
    len_s equ $-separador

section .bss
	a resb 1
    b resb 1
    c resb 2
section .txt
    global _start
_start:
    mov ax,1
L1:
    push ax      ;Iteracion L1
    push ax
    add ax,'0'
    mov [a],ax
    imprimir separador,len_s
    pop ax   
    mov cx,1
L2: 
    push cx         ;Iteracion L2
    
    push cx             ;Guarda CX
    add cx,'0'
    mov [b],cx 
    pop cx              ;Asigna a CX
    push ax             ;Guarda AX
    mul cx

    add al,'0'
    add ah,'0'
    mov [c],ah
    mov [c],al
    
    call print 
    pop ax              ;Asigna a AX

    pop cx          ;Iteracion 2
    inc cx
    cmp cx,10
    jnz L2

    pop ax         ;IteracionL1
    inc ax
    cmp ax,10
    jnz L1
    jmp salir
    
print: 
    imprimir a,1
    imprimir msj1,len_msj1
    imprimir b,1
    imprimir msj2,len_msj2
    imprimir c,2
    imprimir salto,len_salto
    ret

salir:
    mov eax,1
    int 80h