section .data
        arreglo db 3,2,0,7,5
        len_arreglo equ $-arreglo
        new_line db '',10
        len_new_line equ $-new_line

section .bss

        dato resb 1
section .text
        global _start
_start:
        mov esi, arreglo;esi = fijar el tamanio real de alrreglo, posicionar el arreglo en memoria
        mov edi, 0;edi = contener el indice del arreglo

imprimir:     
        mov al,[esi]
        add al, '0'
        mov [dato], al 

        add esi, 1
        add edi, 1

        mov eax, 4
        mov ebx, 1
        mov ecx, dato
        mov edx, 1
        int 80h

        mov eax, 4
        mov ebx, 1
        mov ecx, new_line
        mov edx, len_new_line
        int 80h

        cmp edi, len_arreglo    ;cmp 3,4 => se activa carry
                                ;cmp 4,3 => desactiva carry y zero
                                ;cmp 3,3 => desactiva carry y zer esta activada              
        jb imprimir             ;Cunado la bandera de carry esta activada

salir:
        mov eax, 1
        int 80h