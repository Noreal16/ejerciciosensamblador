%macro escritura 2
        mov eax, 4
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro 

section .data
    repeticion db 'santiago',10
    len_repeticion equ $-repeticion
    new_line db ' ',10
section .bss
    item  resb 2
    multi resb 2
section .text
    global _start
_start:
    mov rcx, 9
    mov rax, 2
for:
    push rcx

    mul rcx
    add rax, '0'
    mov [multi], rax
    ;mov [item], ebx

    ;escritura item, 2
    escritura multi, 2
    escritura new_line, 1
    
    pop rcx
    loop for

salir:
    mov eax, 1
    int 80h

