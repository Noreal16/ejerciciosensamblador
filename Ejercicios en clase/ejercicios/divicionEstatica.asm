;Div de dos numero estaticos
section .data
   resultado db 'El resultado es:',10
   len_resultado equ $-resultado


section .bss
    divicion resb 2 ;cuando no se nececita del enter
section .text
    global _start
_start:
    mov eax,5
    mov ebx,2
    
    
    div ebx ;eax=eax*ebx
    add eax,'0' ;Ajuste para que haga la suma en ascii

    mov [divicion], eax

    mov eax, 4
    mov ebx, 1
    mov ecx, resultado
    mov edx, len_resultado
    int 80h

    mov eax, 4
    mov ebx, 1
    mov ecx, divicion
    mov edx, 2
    int 80h

    mov eax, 1
    int 80h
