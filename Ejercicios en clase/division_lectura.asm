;Leer 2 valores por teclado y dividir 
section .data
        valorA db 'Ingrese primer valor', 10
        len_valorA equ $-valorA

        valorB db 'Ingrese segundo valor', 10
        len_valorB equ $-valorB

        cociente db 'El cociente de division es', 10
        len_cociente equ $-cociente

        residuo db 'El residuo de la division es', 10
        len_residuo equ $-residuo

        new_line db " ",10
        len_new_line equ $-new_line

section .bss
        numeroA resb 2
        numeroB resb 2
        cocint resb 2
        resid resb 2

section .text
        global _start
_start:
;********************Imprime Mensaje*************************
        mov eax, 4
        mov ebx, 1
        mov ecx, valorA
        mov edx, len_valorA
        int 80h
;*******************Lee valores***************************
        mov eax, 3
        mov ebx, 2
        mov ecx, numeroA
        mov edx, 2
        int 80h
;********************Imprime Mensaje*************************
        mov eax, 4
        mov ebx, 1
        mov ecx, valorB
        mov edx, len_valorB
        int 80h
;*******************Lee valores***************************
        mov eax, 3
        mov ebx, 2
        mov ecx, numeroB
        mov edx, 2
        int 80h
;*******************Operacion****************************
        mov al, [numeroA]
        sub al, '0'
        mov bl, [numeroB]
        sub bl, '0'
        div bl

        add al, '0'
        mov [cocint], al
        add ah, '0'
        mov [resid], ah

;******************Imprime el valor de la division ****************************
;******************Imprime el valor del cociente   ****************************
        mov eax, 4
        mov ebx, 1
        mov ecx, cociente
        mov edx, len_cociente
        int 80h

        mov eax, 4
        mov ebx, 1
        mov ecx, cocint
        mov edx, 2
        int 80h

        mov eax, 4
        mov ebx, 1
        mov ecx, new_line
        mov edx, len_new_line
        int 80h
;******************Imprime el valor del residuo ****************************
        mov eax, 4
        mov ebx, 1
        mov ecx, residuo
        mov edx, len_residuo
        int 80h

        mov eax, 4
        mov ebx, 1
        mov ecx, resid
        mov edx, 2
        int 80h

        mov eax, 4
        mov ebx, 1
        mov ecx, new_line
        mov edx, len_new_line
        int 80h

;**********************Salida *************************
salir:  
        mov eax, 1
        int 80h
