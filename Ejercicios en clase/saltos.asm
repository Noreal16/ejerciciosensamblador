;Alex Santiago Nole Reyes
;fecha: 22-05-2020

;Operaciones  Dinamicas Macros
;macros se los utiliza para entradas y salidas

;macro para la presentar
%macro escritura 2
        mov eax, 4
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro
;macro para la lectura por teclado 
%macro lectura 2
        mov eax, 3
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro


section .data

        dato1 db 'ingrese primer valor',10
        len_dato1 equ $-dato1

        dato2 db 'ingrese segundo valor', 10
        len_dato2 equ $-dato2
    
        resultado_suma db 'El resultado de la suma es', 10
        len_resultado_suma  equ $-resultado_suma

        resultado_resta db 'El resultado de la resta es:',10
        len_resultado_resta equ $-resultado_resta

        resultado_multiplicacion db 'El resultado de la multiplicacion es:',10
        len_resultado_mul equ $-resultado_multiplicacion

        resultado_cociente db 'El resultado de la cociene es:',10
        len_resultado_cociente equ $-resultado_cociente
   
        resultado_residuo db 'El resultado de la residuo es:',10
        len_resultado_residuo equ $-resultado_residuo
    
        new_line db " ",10
        len_new_line equ $-new_line

section .bss

        valor1 resb 2
        valor2 resb 2

        suma resb 1
        resta resb 1
        mult resb 1
        cociete resb 1
        resid resb 1

section .text
        global _start
_start:

        ;************************Presentar por npantalla mensaje 1 **************************
        escritura dato1, len_dato1
        ;*************************Leer por pantalla primer valor ****************************
        lectura valor1, 2
        ;************************Presentar por npantalla mensaje 2 **************************
        escritura dato2, len_dato2
        ;*************************Leer por pantallan segundo valor **************************
        lectura valor2, 2
        ;***************************saltos*************************************************
        jmp dividir
        ;*************************suma valores***********************************************
    sumar:
        mov al, [valor1]
        sub al, '0'
        mov bl, [valor2]
        sub bl, '0'

        add al, bl
        add al, '0'
        mov [suma], al
        ;************************Presentar por npantalla resultado de la suma******************
        escritura resultado_suma, len_resultado_suma
        escritura suma, 1
        escritura new_line, len_new_line

        jmp restar

        ;*************************resta valores************************************************
    restar:    
        mov al, [valor1]
        sub al, '0'
        mov bl, [valor2]
        sub bl, '0'

        sub al, bl
        add al, '0'
        mov [resta], al
        ;************************Presentar por npantalla resultado de la resta **************************
        escritura resultado_resta, len_resultado_resta
        escritura resta, 1
        escritura new_line, len_new_line

        jmp multiplicar
        ;*************************multiplicacion de valores***************************
    multiplicar:
        mov al, [valor1]
        sub al, '0'
        mov bl, [valor2]
        sub bl, '0'

        mul ebx
        add al, '0'
        mov [mult], al
        ;************************Presentar por pantalla resultado multiplicacion **************************
        escritura resultado_multiplicacion, len_resultado_mul
        escritura mult, 1
        escritura new_line, len_new_line

        jmp salir

        ;*************************division valores********************************
    dividir:
        mov al, [valor1]
        sub al, '0'
        mov bl, [valor2]
        sub bl, '0'

        div bl
        add al, '0'
        mov [cociete], al
        add ah, '0'
        mov [resid], ah
        ;************************Presentar por pantalla cociente ******************
        escritura resultado_cociente, len_resultado_cociente
        escritura cociete, 1
        escritura new_line, len_new_line
        ;************************Presentar por pantalla residuo *******************
        escritura resultado_residuo, len_resultado_residuo
        escritura resid, 1
        escritura new_line, len_new_line

        jmp sumar

        ;*************************Salida*******************************************
    salir:
        mov eax, 1
        int 80h