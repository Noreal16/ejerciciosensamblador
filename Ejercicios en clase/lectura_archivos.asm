%macro escribir 2 
        mov eax, 4
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro

segment .data
        msj1 db "Leer  el archivo",10
        len1 equ $ -msj1

        archivo db "/home/santy/Escritorio/ejerciciosensamblador/archivos/archivo2.txt"
segment .bss
        texto resb 30
        idarchivo resd 1
        
segment .text 
        global _start

_start:
        ;*****************Abrir el archivo****************************************
        mov eax, 5              ;establece(definimos) el servicio para crear archivos
                                ;permite hacer open en el archivo
        mov ebx, archivo        ;direccion del archivo
        mov ecx, 0              ;definimos modo de acceso
        mov edx, 777h           ;se asigna los permisos al archivo
        int 80h

        test eax, eax           ;test hace una comparacion logica & entre dos operandos ...Para verificar si el archivo se creo de manera correcata o incorrecta
        jz salir                ; se ejecuta cuando tiene errores en el archivo

        mov dword [idarchivo], eax ;mandatorio para poder trabajar en archivos
        escribir msj1, len1
        ;**********************leer el archivo**********************************
        mov eax, 3
        mov ebx, [idarchivo]
        mov edx, texto
        mov edx, 15
        int 80h

        escribir texto, 15
        ;***********************cerrar el Archivo****************************
        mov eax, 6              ;clode
        mov ebx, [idarchivo]
        mov ecx, 0
        mov edx, 0
        int 80h
salir:
    mov eax,1
    int 80h
