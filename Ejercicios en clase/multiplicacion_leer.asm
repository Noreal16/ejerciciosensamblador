;Leer 2 valores por teclado y multiplicar 
section .data
    valorA db 'Ingrese primer valor', 10
    len_valorA equ $-valorA

    valorB db 'Ingrese segundo valor', 10
    len_valorB equ $-valorB

    resultado db 'El resultado de multiplicar es', 10
    len_resultado equ $-resultado

    new_line db " ",10
    len_new_line equ $-new_line

section .bss
 numeroA resb 2
 numeroB resb 2
 result resb 5

section .text
global _start
_start:
;********************Imprime Mensaje*************************
 mov eax, 4
 mov ebx, 1
 mov ecx, valorA
 mov edx, len_valorA
 int 80h
;*******************Lee valores***************************
mov eax, 3
mov ebx, 2
mov ecx, numeroA
mov edx, 2
int 80h
;********************Imprime Mensaje*************************
 mov eax, 4
 mov ebx, 1
 mov ecx, valorB
 mov edx, len_valorB
 int 80h
;*******************Lee valores***************************
mov eax, 3
mov ebx, 2
mov ecx, numeroB
mov edx, 2
int 80h
;*******************Operacion****************************
mov al, [numeroA]
sub al, '0'
mov bl, [numeroB]
sub bl, '0'

mul bl
add ax, '0'

mov [result], ax
;******************Imprime el valor de la multiplicacion es ***********
mov eax, 4
mov ebx, 1
mov ecx, resultado
mov edx, len_resultado
int 80h

mov eax, 4
mov ebx, 1
mov ecx, result
mov edx, 5
int 80h

mov eax, 4
mov ebx, 1
mov ecx, new_line
mov edx, len_new_line
int 80h

;**********************Salida *************************
mov eax, 1
int 80h
