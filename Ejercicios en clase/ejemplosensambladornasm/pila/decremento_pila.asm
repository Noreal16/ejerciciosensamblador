; un programa que muestra la funcionalidad DEC para decrementar
; e imprimir el resultado en pantalla
%macro imprimirdato 2
    mov eax,4
    mov ebx,1
    mov ecx,%1
    mov edx,%2
    int 80h
%endmacro
section .data
	msj db "Ingrese un número:"
	len equ $ - msj
	new_line db '',10

section .bss
	numero resb 2
	resultado resb 1
	
section .text
	global _start

_start :
	; ******* mensaje ingrese número
	imprimirdato msj,len

	;___________Leer numero____________
	mov eax, 3
	mov ebx, 2
	mov ecx, numero
	mov edx, 1
	int 80h

	jmp imprimir

imprimir:
	mov eax, [numero]
	push rax

	;____________imprimir el numero__________
	imprimirdato numero,1
	;___________imprime salto de linea__________
	imprimirdato new_line,1

	pop rax
	sub eax, '0'
	dec eax	
	mov ecx, eax
	add eax, '0'
	mov [numero], eax	

	cmp ecx, 0
	jz salir
	jmp imprimir

salir:
	mov eax, 1
	int 80h
