%macro imprimir 2
    mov eax,4
    mov ebx,1
    mov ecx,%1
    mov edx,%2
    int 80h
%endmacro
%macro leer 2
    mov eax,3		
	mov ebx,2		
	mov ecx,%1	
	mov edx,%2    
	int 80H
%endmacro

section .data
        num1 db "Ingrese un numero: "
        len_num1 equ $-num1
        ; Primer dato 
        msg db "El  primer valor extraido de la pila es: "
        len_msg equ $-msg

        new_line db "",10
        len_new_line equ $-new_line
section .bss 
        numero resd 1
        datopila resb 1
        datopila1 resb 1
        datopila2 resb 1
        datopila3 resb 1
        datopila4 resb 1
        datopila5 resb 1
        datopila6 resb 1

section .text
        global _start
_start:
    ;ingresados____
        imprimir num1, len_num1
        leer numero,2
        mov ax,[numero]
        sub ax,'0'            ; muevo el valor de 9 a l registro ax   
        push ax             ; meto el valor a la pila que contiene (9)
        
        imprimir num1, len_num1
        leer numero,2
        mov ax,[numero]
        sub ax,'0'         ; muevo el valor de 9 a l registro ax   
        push ax 

        imprimir num1, len_num1
        leer numero,2
        mov ax,[numero] 
        sub ax,'0'          ; muevo el valor de 9 a l registro ax   
        push ax 

        imprimir num1, len_num1
        leer numero,2
        mov ax,[numero]  
        sub ax,'0'        ; muevo el valor de 9 a l registro ax   
        push ax 


        pop cx              ; extraigo el valor  de la punta de la pila  y lo ubico en cx
        add cx,'0'         ; tranformo los digitos a caracter para presentar.
        mov [datopila],cx   ; muevo el caracter cx en la variable  datopila

        pop cx              ; extraigo el valor  de la punta de la pila  y lo ubico en cx
        add  cx,'0'         ; tranformo los digitos a caracter para presentar.
        mov [datopila1],cx   ; muevo el caracter cx en la variable  datopila

        pop cx              ; extraigo el valor  de la punta de la pila  y lo ubico en cx
        add  cx,'0'         ; tranformo los digitos a caracter para presentar.
        mov [datopila2],cx   ; muevo el caracter cx en la variable  datopila
        
        pop cx              ; extraigo el valor  de la punta de la pila  y lo ubico en cx
        add cx,'0'         ; tranformo los digitos a caracter para presentar.
        mov [datopila3],cx   ; muevo el caracter cx en la variable  datopila
        

    ; Mensaje en pantalla con macros primer mensaje
      imprimir msg,len_msg
    ; imprimi el valor almacenado en la pila primer dato
      imprimir datopila,1
    ; ahora como mando otros parametros lo ocupo para saltar de linea.
      imprimir new_line,len_new_line


    ;*************Extraigo el segundo valor de la pila.
    
    ; Mensaje en pantalla con macros primer mensaje
      imprimir msg,len_msg
    ; imprimi el valor almacenado en la pila primer dato
      imprimir datopila1,1
    ; ahora como mando otros parametros lo ocupo para saltar de linea.
      imprimir new_line,len_new_line

    ;*************Extraigo el tercer valor de la pila.
    
    ; Mensaje en pantalla con macros primer mensaje
      imprimir msg,len_msg
    ; imprimi el valor almacenado en la pila primer dato
      imprimir datopila2,1
    ; ahora como mando otros parametros lo ocupo para saltar de linea.
      imprimir new_line,len_new_line


    ; Mensaje en pantalla con macros primer mensaje
      imprimir msg,len_msg
    ; imprimi el valor almacenado en la pila primer dato
      imprimir datopila3,1
    ; ahora como mando otros parametros lo ocupo para saltar de linea.
      imprimir new_line,len_new_line


      pop bx              ; extraigo el valor  de la punta de la pila  y lo ubico en cx
      add bx,'0'         ; tranformo los digitos a caracter para presentar.
      mov [datopila4],bx

      imprimir msg,len_msg
    ; imprimi el valor almacenado en la pila primer dato
      imprimir datopila4,1
    ; ahora como mando otros parametros lo ocupo para saltar de linea.
      imprimir new_line,len_new_line


      add bx,'0'         ; tranformo los digitos a caracter para presentar.
      mov [datopila4],bx

      imprimir msg,len_msg
    ; imprimi el valor almacenado en la pila primer dato
      imprimir datopila4,1
    ; ahora como mando otros parametros lo ocupo para saltar de linea.
      imprimir new_line,len_new_line
     ;Cierro el programa

       add bx,'0'         ; tranformo los digitos a caracter para presentar.
      mov [datopila4],bx

      imprimir msg,len_msg
    ; imprimi el valor almacenado en la pila primer dato
      imprimir datopila4,1
    ; ahora como mando otros parametros lo ocupo para saltar de linea.
      imprimir new_line,len_new_line
     ;Cierro el programa

      add bx,'0'         ; tranformo los digitos a caracter para presentar.
      mov [datopila4],bx

      imprimir msg,len_msg
    ; imprimi el valor almacenado en la pila primer dato
      imprimir datopila4,1
    ; ahora como mando otros parametros lo ocupo para saltar de linea.
      imprimir new_line,len_new_line
     ;Cierro el programa
        
    mov eax,1
    int 80h