%macro imprimir 2    ; para imprimir y para salto de linea
     mov eax,4
     mov ebx,1
     mov ecx,%1
     mov edx,%2
     int 80h
%endmacro

section .data
       
        ; Primer dato 
        resultado db "El  primer valor extraido de la pila es: "
        len_resultado equ $-resultado
        resultado1 db "El segundo valor extraido de la pila es: "
        len_resultado1 equ $-resultado1
        resultado2 db "El tercer valor extraido de la pila es: "
        len_resultado2 equ $-resultado2
        resultado3 db "El cuarto valor extraido de la pila es: "
        len_resultado3 equ $-resultado3
        
        new_line db "",10
        len_new_line equ $-new_line
section .bss 
         
        datopila resb 1
        datopila1 resb 1
        datopila2 resb 1
        datopila3 resb 1

section .text
        global _start
_start:


        mov ax,9            
        push ax             
        
        mov ax,8           
        push ax 

        mov ax,7            
        push ax 


        pop cx              
        add  cx,'0'         
        mov [datopila],cx   

        pop cx              
        add  cx,'0'         
        mov [datopila1],cx   

        pop cx              
        add  cx,'0'         
        mov [datopila2],cx   
        

    
      imprimir resultado,len_resultado
      imprimir datopila,1
      imprimir new_line,len_new_line


      imprimir resultado1,len_resultado1
      imprimir datopila1,1
      imprimir new_line,len_new_line
   
      imprimir resultado2,len_resultado2
      imprimir datopila2,1
      imprimir new_line,len_new_line


     ;Cierro el programa
        mov eax,1
        int 80h