%macro imprimir 2
    mov eax,4
    mov ebx,1
    mov ecx,%1
    mov edx,%2
    int 80h
%endmacro
%macro leer 1
    mov eax,3		
	mov ebx,2		
	mov ecx,%1	
	mov edx,2    
	int 80H
%endmacro 
    
%macro agregar 1
    mov ax,%1
    sub ax,'0'            
    push ax
%endmacro

%macro quitar 1
    pop cx            
    add cx,'0'       
    mov [%1],cx
%endmacro
section .data
        num1 db "Ingrese un numero: "
        len_num1 equ $-num1
        ; Primer dato 
        msg db "El Valor extraido de la pila es: "
        len_msg equ $-msg

        new_line db "",10
        len_new_line equ $-new_line
section .bss 
        numero resd 1
        datopila resb 1
section .text
        global _start
_start:
    ;______________ingresados en la pila____
        imprimir num1, len_num1
        leer numero
        agregar [numero]
        
        imprimir num1, len_num1
        leer numero
        agregar [numero]

        imprimir num1, len_num1
        leer numero
        agregar [numero] 

        imprimir num1, len_num1
        leer numero
        agregar [numero] 

;_______________Quita datos de la pila___________________
        quitar datopila
        imprimir msg,len_msg
        imprimir datopila,1
        imprimir new_line,len_new_line

        quitar datopila
        imprimir msg,len_msg
        imprimir datopila,1
        imprimir new_line,len_new_line

        quitar datopila
        imprimir msg,len_msg
        imprimir datopila,1
        imprimir new_line,len_new_line

        quitar datopila
        imprimir msg,len_msg
        imprimir datopila,1
        imprimir new_line,len_new_line
;_________________Verifcar si existe datos en la PIla.............

        quitar datopila
        imprimir msg,len_msg
        imprimir datopila,1
        imprimir new_line,len_new_line


        quitar datopila
        imprimir msg,len_msg
        imprimir datopila,1
        imprimir new_line,len_new_line

        quitar datopila
        imprimir msg,len_msg
        imprimir datopila,1
        imprimir new_line,len_new_line
        
        quitar datopila
        imprimir msg,len_msg
        imprimir datopila,1
        imprimir new_line,len_new_line
        
    mov eax,1
    int 80h