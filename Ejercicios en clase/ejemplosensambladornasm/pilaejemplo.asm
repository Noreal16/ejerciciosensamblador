%macro imprimir 2    ; para imprimir y para salto de linea
     mov eax,4
     mov ebx,1
     mov ecx,%1
     mov edx,%2
     int 80h
%endmacro

section .data
       
        ; Primer dato 
        resultado db "El  primer valor extraido de la pila es: "
        len_resultado equ $-resultado
        resultado1 db "El segundo valor extraido de la pila es: "
        len_resultado1 equ $-resultado1
        resultado2 db "El tercer valor extraido de la pila es: "
        len_resultado2 equ $-resultado2
        resultado3 db "El cuarto valor extraido de la pila es: "
        len_resultado3 equ $-resultado3
        resultado4 db "El quinto valor extraido de la pila es: "
        len_resultado4 equ $-resultado4


        new_line db "",10
        len_new_line equ $-new_line
section .bss 
         
        datopila resb 1
        datopila1 resb 1
        datopila2 resb 1
        datopila3 resb 1
        datopila4 resb 1

section .text
        global _start
_start:
               
        ;ULTIMO EN ENTRAR PRIMERO EN SALIR
    ; Opero

        mov dx,9            ; muevo el valor de 9 a l registro ax   
        push dx             ; meto el valor a la pila que contiene (9)
        
        mov cx,8            ; muevo el valor de 9 a l registro ax   
        push cx 

        mov ax,7            ; muevo el valor de 9 a l registro ax   
        push ax 

        mov bx,6           ; muevo el valor de 9 a l registro ax   
        push bx 


        pop dx              ; extraigo el valor  de la punta de la pila  y lo ubico en cx
        add  dx,'0'         ; tranformo los digitos a caracter para presentar.
        mov [datopila],dx   ; muevo el caracter cx en la variable  datopila

        pop ax              ; extraigo el valor  de la punta de la pila  y lo ubico en cx
        add  ax,'0'         ; tranformo los digitos a caracter para presentar.
        mov [datopila1],ax   ; muevo el caracter cx en la variable  datopila

        pop cx              ; extraigo el valor  de la punta de la pila  y lo ubico en cx
        add  cx,'0'         ; tranformo los digitos a caracter para presentar.
        mov [datopila2],cx   ; muevo el caracter cx en la variable  datopila
        
        pop bx              ; extraigo el valor  de la punta de la pila  y lo ubico en cx
        add  bx,'0'         ; tranformo los digitos a caracter para presentar.
        mov [datopila3],bx   ; muevo el caracter cx en la variable  datopila
        

    ; Mensaje en pantalla con macros primer mensaje
      imprimir resultado,len_resultado
    ; imprimi el valor almacenado en la pila primer dato
      imprimir datopila,1
    ; ahora como mando otros parametros lo ocupo para saltar de linea.
      imprimir new_line,len_new_line


    ;*************Extraigo el segundo valor de la pila.
    
    ; Mensaje en pantalla con macros primer mensaje
      imprimir resultado1,len_resultado1
    ; imprimi el valor almacenado en la pila primer dato
      imprimir datopila1,1
    ; ahora como mando otros parametros lo ocupo para saltar de linea.
      imprimir new_line,len_new_line

    ;*************Extraigo el tercer valor de la pila.
    
    ; Mensaje en pantalla con macros primer mensaje
      imprimir resultado2,len_resultado2
    ; imprimi el valor almacenado en la pila primer dato
      imprimir datopila2,1
    ; ahora como mando otros parametros lo ocupo para saltar de linea.
      imprimir new_line,len_new_line


    ; Mensaje en pantalla con macros primer mensaje
      imprimir resultado3,len_resultado3
    ; imprimi el valor almacenado en la pila primer dato
      imprimir datopila3,1
    ; ahora como mando otros parametros lo ocupo para saltar de linea.
      imprimir new_line,len_new_line


      pop bx              ; extraigo el valor  de la punta de la pila  y lo ubico en cx
      add bx,'0'         ; tranformo los digitos a caracter para presentar.
      mov [datopila4],bx

      imprimir resultado4,len_resultado4
    ; imprimi el valor almacenado en la pila primer dato
      imprimir datopila4,1
    ; ahora como mando otros parametros lo ocupo para saltar de linea.
      imprimir new_line,len_new_line
     ;Cierro el programa
        mov eax,1
        int 80h