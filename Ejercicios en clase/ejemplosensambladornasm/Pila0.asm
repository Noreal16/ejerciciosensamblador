%macro imprimir 2    ; para imprimir y para salto de linea
     mov eax,4
     mov ebx,1
     mov ecx,%1
     mov edx,%2
     int 80h
%endmacro

section .data
       
        ; Primer dato 
        resulBX db "Presentamos el valor BX: "
        len_resulBX equ $-resulBX
        resulAX db "Presentamos el valor AX: "
        len_resulAX equ $-resulAX
        
        new_line db "",10
        len_new_line equ $-new_line
section .bss 
         
        valBX resb 1
        valAX resb 1

section .text
        global _start
_start:
               
    Mov AX,5    ;AX=5 
    Mov BX,9    ;BX=9 
    Push AX     ;Pila=5 
    Mov AX,BX   ;AX=9 

    ;_____________Presentar el valor de Ax
    add AX,'0'
    mov [valAX],AX
    imprimir resulAX, len_resulAX
    imprimir valAX,1
    imprimir new_line,len_new_line



    Pop BX      ;BX=5 

    ;______________Presentar el valor de BX
    add BX,'0'
    mov [valBX],BX
    imprimir resulBX, len_resulBX
    imprimir valBX,1
    imprimir new_line,len_new_line


     ;Cierro el programa
    mov eax,1
    int 80h