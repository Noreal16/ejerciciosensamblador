section .data
    mensaje db 'Ingrese  numero', 10
    len_mensaje equ $-mensaje

    mensaje1 db 'Ingresese numero',10
    len_mensaje1 equ $-mensaje1

    imprime_Resultado db 'El valor de  suma Es ', 10
    len_presentado equ $-imprime_Resultado
section .bss
    numero resb 5 ; reserva 5 espacion caracteres
    numero1 resb 5 ;reserva 5 en espacios caracteres
    suma resb 5     ;reserva un valor para la suma
section .text
    global _start
_start:
    ;***************************Imprime mensaje**************
    mov eax, 4
    mov ebx, 1
    mov ecx, mensaje
    mov edx, len_mensaje
    int 80h
    ;-------------------------Asignacion de un numero-------------------------
     mov eax, 3
    mov ebx, 2
    mov ecx, numero
    mov edx, 5
    int 80h
    ;**************************Mensaje presentacion********************
    mov eax, 4
    mov ebx, 1
    mov ecx, mensaje1
    mov edx, len_mensaje1
    int 80h
    ;-------------------------Asignacion de un numero-------------------------
    mov eax, 3
    mov ebx, 2
    mov ecx, numero1
    mov edx, 5
    int 80h
    ;--------------------------Suma Operacion---------------------------------
    mov eax, [numero]
    sub eax, '0'
    mov ebx, [numero1]
    sub ebx,'0'
    add eax, ebx
    add eax,'0'

    mov [suma], eax
    ;**************************Imprime numero********************
    
    mov eax, 4
    mov ebx, 1
    mov ecx, imprime_Resultado
    mov edx, len_presentado
    int 80h
    ;***************************************************************
    mov eax, 4
    mov ebx, 1
    mov ecx, suma
    mov edx, 5
    int 80h
    ;********************Salir del sistema*******************
    mov eax, 1
    int 80h