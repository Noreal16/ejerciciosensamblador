section .data
    mensaje db 'ingrese un numero', 10
    len_mensaje equ $-mensaje
    menjase_presentacion db 'El numero ingresado Es', 10
    len_presentado equ $-menjase_presentacion
section .bss
    numero resb 5 ; reserva 5 espacion caracteres

section .text
    global _start
_start:
    ;***************************Imprime mensaje**************
    mov eax, 4
    mov ebx, 1
    mov ecx, mensaje
    mov edx, len_mensaje
    int 80h
    ;-------------------------Asignacion de un numero-------------------------
    mov ebx, 7
    add ebx, '0'
    mov [numero], ebx
    ;**************************Mensaje presentacion********************
    mov eax, 4
    mov ebx, 1
    mov ecx, menjase_presentacion
    mov edx, len_presentado
    int 80h
    ;**************************Imprime numero********************
    mov eax, 4
    mov ebx, 1
    mov ecx, numero
    mov edx, 5
    int 80h
    ;********************Salir del sistema*******************
    mov eax, 1
    int 80h