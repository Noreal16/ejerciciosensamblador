%macro escritura 2
        mov eax, 4
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro

section .data
        msj1 db ' * '
        len_msj1 equ $-msj1

        msj2 db ' = '
        len_msj2 equ $-msj2

        new_line db 10,''
        len_new_line equ $-new_line
section .bss
        a resb 2
        b resb 2
        c resb 2 
section .text
        global _start
_start:
        mov al, 3
        add al, '0'
        mov [a], al
        mov cx, 1
ciclo:
        push cx
        mov ax, [a]
        sub ax, '0'
        mul cx
        add ax, '0'
        mov [c], ax
        add cx,'0'
        mov [b], cx
        ;Llamados de los mensajes
        call imprimir_numero_a
        call imprimir_numero_msj1
        call imprimir_numero_b
        call imprimir_numero_msj2
        call imprimir_numero_resultado
        call imprimir_numero_enter
        
        pop cx
        inc cx
        cmp cx, 10
        jnz ciclo

imprimir_numero_a:
        mov eax, 4
        mov ebx, 1
        mov ecx, a
        mov edx, 2
        int 80h
        ret
imprimir_numero_msj1:
        mov eax, 4
        mov ebx, 1
        mov ecx, msj1
        mov edx, len_msj1
        int 80h
        ret
imprimir_numero_b:
        mov eax, 4
        mov ebx, 1
        mov ecx, b
        mov edx, 2
        int 80h
        ret
imprimir_numero_msj2:
        mov eax, 4
        mov ebx, 1
        mov ecx, msj2
        mov edx, len_msj2
        int 80h
        ret
imprimir_numero_resultado:
        mov eax, 4
        mov ebx, 1
        mov ecx, c
        mov edx, 2
        int 80h
        ret
imprimir_numero_enter:
        mov eax, 4
        mov ebx, 1
        mov ecx, new_line
        mov edx, len_new_line
        int 80h
        ret
salir:
        mov eax, 1
        int 80h