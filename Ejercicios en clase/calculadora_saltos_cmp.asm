;Ejercicio calculadora saltos y cmp
;Alex Nole
;13-julio-2020

;macro para la presentar
%macro escritura 2
        mov eax, 4
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro
;macro para la lectura por teclado 
%macro lectura 2
        mov eax, 3
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro


section .data
	msg1		db		10,'-Calculadora-',10,0
	lmsg1		equ		$ - msg1
 
	msg2		db		10,'Numero 1: ',0
	lmsg2		equ		$ - msg2
 
	msg3		db		'Numero 2: ',0
	lmsg3		equ		$ - msg3

	msg4 		db		10,'1. sumar',10,0
	lmsg4		equ		$-msg4

	msg5		db		10,'2. restar',10,0
	lmsg5		equ		$-msg5

	msg6 		db		10,'3. multiplicar',10,0
	lmsg6		equ		$-msg6

	msg7 		db		10,'4. dividir',10,0
	lmsg7		equ		$-msg7

	msg8 		db		10,'5. salir',10,0
	lmsg8		equ		$-msg8

	msg9		db		10,'Resultado: ',0
	lmsg9		equ		$ - msg9
 
	msg10		db		10,'Opcion Invalida',10,0
	lmsg10		equ		$ - msg10
	
	msg11		db		10,'Seleccione opcion',10,0
	lmsg11		equ		$ - msg11
 
	nlinea		db		10,10,0
	lnlinea		equ		$ - nlinea
 
section .bss
  	opcions:    resb    2
	num1:		resb	2
	num2:		resb 	2
	resultado:	resb 	2
 
section .text
 	global _start
 
_start:
	;************************Mensaje de encabezado del ejercicio****************************
	; Imprimimos en pantalla el mensaje 1
	escritura msg1, lmsg1
	
	;********************** Imprimimos en pantalla el mensaje 2
	escritura msg2, lmsg2
	
	; Obtenemos el numero 1
	lectura num1, 2
	
	;********************** Imprimimos en pantalla el mensaje 3
	escritura msg3, lmsg3
	
	; Obtenemos el numero 2
	lectura num2, 2
	
	;***************Mensajes del menú*****************
	;***************Mensajes del Sumar*****************
MENU:
    escritura msg4, lmsg4
	;***************Mensajes del Restar*****************
	escritura msg5, lmsg5
	;***************Mensajes del Multiplicar*****************
	escritura msg6, lmsg6
	;***************Mensajes del Dividir*****************
	escritura msg7, lmsg7
 	;***************Mensajes del Salir*****************
	escritura msg8, lmsg8
	;*************Mensaje de Opcion******************
	escritura msg11, lmsg11
	
	lectura opcions, 2

	mov ah, [opcions]
	sub ah, '0'

	;******************Comparar el valor ingresado por el usuario, para saber que operación se va a utilizar
	;JE = jmp(condicion) if exist igualdad  salta cuando los valores de los dos operandos son iguales ZF= 1 y CF =0
	cmp ah, 1
	je sumar
 
	cmp ah, 2
	je restar

	cmp ah, 3
	je multiplicar

	cmp ah, 4
	je dividir

	cmp ah, 5
	je salir

 
	;jmp dividir  
 
	; Si el valor ingresado no cumple con ninguna de las condiciones anteriores entonces mostramos un mensaje de error y finalizamos
	; la ejecucion del programa
	escritura msg10, lmsg10
	
	
 
 sumar:
	; Movemos los numeros ingresados a los registro AL y BL
	mov al, [num1]
	mov bl, [num2]
 
	; Convertimos los valores ingresados de ascii a decimal
	sub al, '0'
	sub bl, '0'
 
	; Sumamos el registro AL y BL
	add al, bl
;	aaa
 
	; Convertimos el resultado de la suma de decimal a ascii
	add al, '0'
 
	; Movemos el resultado a un espacio reservado en la memoria
	mov [resultado], al
 
	; Imprimimos en pantalla el mensaje 9
	escritura msg9, lmsg9
 
	; Imprimimos en pantalla el resultado
	escritura resultado, 2
	;jmp restar
	jmp MENU
 
restar:
	; Movemos los numeros ingresados a los registro AL y BL
	mov al, [num1]
	mov bl, [num2]
 
	; Convertimos los valores ingresados de ascii a decimal
	sub al, '0'
	sub bl, '0'
 
	; Restamos el registro AL y BL
	sub al, bl
 
	; Convertimos el resultado de la resta de decimal a ascii
	add al, '0'
 
	; Movemos el resultado a un espacio reservado en la memoria
	mov [resultado], al
 
	; Imprimimos en pantalla el mensaje 9
	escritura msg9, lmsg9
 
	; Imprimimos en pantalla el resultado
	escritura resultado, 2
 
	;jmp multiplicar

	jmp MENU
multiplicar:
 
	; Movemos los numeros ingresados a los registro AL y BL
	mov al, [num1]
	mov bl, [num2]
 
	; Convertimos los valores ingresados de ascii a decimal
	sub al, '0'
	sub bl, '0'
 
	; Multiplicamos. AX = AL X BL
	mul bl
 
	; Convertimos el resultado de la resta de decimal a ascii
	add ax, '0'
 
	; Movemos el resultado a un espacio reservado en la memoria
	mov [resultado], ax
 
	; Imprimimos en pantalla el mensaje 9
	escritura msg9, lmsg9
 
	; Imprimimos en pantalla el resultado
	escritura resultado, 2
 
	;jmp salir

	jmp MENU
dividir:
 
	; Movemos los numeros ingresados a los registro AL y BL
	mov al, [num1]
	mov bl, [num2]
 
	; Igualamos a cero los registros DX y AH
	mov dx, 0
	mov ah, 0
 
	; Convertimos los valores ingresados de ascii a decimal
	sub al, '0'
	sub bl, '0'
 
	; Division. AL = AX / BL. AX = AH:AL
	div bl
 
	; Convertimos el resultado de la resta de decimal a ascii
	add ax, '0'
 
	; Movemos el resultado a un espacio reservado en la memoria
	mov [resultado], ax
 
	; Imprimimos en pantalla el mensaje 9
	escritura msg9, lmsg9
 
	; Imprimimos en pantalla el resultado
	escritura resultado, 2
 
	;jmp sumar
	jmp MENU
salir:
	; Imprimimos en pantalla dos nuevas lineas
	escritura nlinea, lnlinea
	
	; Finalizamos el programa
	mov eax, 1
	mov ebx, 0
	int 80h
