;Alex Santiago Nole Reyes
; 6 A
; Tabla de multiplicar completa
;5-08-2020
%macro escritura 2
        mov eax, 4
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro

section .data
        msj1 db ' * '
        len_msj1 equ $-msj1

        msj2 db ' = '
        len_msj2 equ $-msj2

        new_line db 10, ' '
        len_new_line equ $-new_line

section .bss

        valor_a resb 2
        valor_b resb 2
        resultado resb 2

section .text
        global _start
_start:

        mov ax, 1
        add ax, '0'
        mov [valor_a], ax
        mov cx, 1

ciclo1:
        push cx
        mov  ax, [valor_a]
        sub  ax, '0'
        mul  cx
        add  ah, '0'
        add  al, '0'
        mov  [resultado], ah
        mov  [resultado], al
        add  cx, '0'
        mov  [valor_b], cx

        ;Llamamos a los mensajes
        escritura valor_a, 2
        escritura msj1, len_msj1
        escritura valor_b, 2
        escritura msj2, len_msj2
        escritura resultado, 2
        escritura new_line, len_new_line
        
        pop cx
        inc cx          ;incrementa el valor cx
        cmp cx, 10
        jnz ciclo1
        jz  ciclo2
        ret

ciclo2:
        escritura new_line, len_new_line
        mov  ax, [valor_a]
        sub  ax, '0'     ;cambiamos el valor de cadena a entero
        push ax          ;agrgamos valor en pils
        inc  ax          ;incrementa el valor de ax
        add  ax, '0'
        mov  [valor_a], ax 
        pop  ax           ;obtiene el valor de la cima de la pila y es evaluado por el cmp
        mov  cx, 1
        cmp  ax, 9        ;comprara si el valor de ax = 9
        jg   salir
        jnz  ciclo1
        ret
salir:
        mov eax, 1
        int 80h