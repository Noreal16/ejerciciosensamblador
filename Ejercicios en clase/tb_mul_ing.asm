%macro escritura 2
        mov eax, 4
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro

section .data
        msj1 db ' * '
        len_msj1 equ $-msj1

        msj2 db ' = '
        len_msj2 equ $-msj2

        new_line db 10, ' '
        len_new_line equ $-new_line

section .bss

        numero resb 2
        valor_a resb 2
        valor_b resb 2
        resultado resb 2
        aux resb 2
        x resb 2

section .text
        global _start
_start:
        mov al, 1
        mov [aux], al
        mov ecx, 0
principal:
        cmp ecx,9
        jz bucle
        inc ecx
        push ecx
        mov [numero], ecx
        jmp imp 
imp:
        mov al,[numero]
        mov [valor_a],al
        mov cl,[aux]
        mul cl

        mov [numero], cl
        mov ah, [resultado]
        add ax, '00'
        add cx, '00'
        mov [resultado],ah
        mov [resultado],cl

        escritura valor_a, 2
        escritura msj1, len_msj1
        escritura valor_b, 2
        escritura msj2, len_msj2
        escritura resultado, 2
        escritura new_line, len_new_line
        add eax,48
        add [numero],eax
        escritura numero,2
        escritura new_line, len_new_line
        pop ecx
        jmp principal
bucle:
        escritura new_line, len_new_line
        mov ebx, [aux]
        inc ebx
        mov [aux], ebx
        mov ecx, '0'
        cmp ebx, 10
        jb  principal    ;jnAE para comprar valors menors o diferentes
salir:
        mov eax,1
        int 80h