; Alex  Santiago Nole Reyes
; 6 A
; Tabla de multiplicar Completa

%macro escritura 2
        mov eax, 4
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro

section .data
        msj1 db ' * '
        len_msj1 equ $-msj1

        msj2 db ' = '
        len_msj2 equ $-msj2

        new_line db 10,''
        len_new_line equ $-new_line

section .bss
        valor_a resb 2
        valor_b resb 2
        resultado resb 2
section .text
    global _start
_start:

        mov ax, 1
        
ciclo1:
        push ax

        push ax
        add ax, '0'
        mov [valor_a], ax
        pop ax
        mov cx, 1
ciclo2:
        push cx

        push cx
        add cx, '0'
        mov [valor_b], cx
        pop cx
        push ax
        mul cx
        
        add ax, '0'
        mov [resultado], ah
        add al, '0'
        mov [resultado], al

        escritura valor_a, 2
        escritura msj1, len_msj1
        escritura valor_b, 2
        escritura msj2, len_msj2
        escritura resultado, 2
        escritura new_line, len_new_line

        pop ax
        pop cx
        inc cx
        cmp cx, 10
        jnz ciclo2


        pop ax
        inc ax
        cmp ax, 10
        
        jnz ciclo1
        jmp salir

        
        
salir:
        mov eax, 1
        int 80h