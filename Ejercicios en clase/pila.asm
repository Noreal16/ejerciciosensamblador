;programa que imprima **********************

section .data
        asterisco db '*'
section .text
 	global _start

_start:
        mov rcx, 9 

imprimir:
        dec rcx
        push rcx  ;con el dec va decrementando en valor en pila 1

        mov rax, 4
        mov rbx, 1
        mov rcx, asterisco
        mov rdx, 1
        int 80h

        pop rcx
        cmp rcx, 0
        jnz imprimir ; cuando no sea 0 imprime 
        ;jmp imprimir

        mov rax,1
        int 80h