;Multiplicacion de dos numero estaticos
section .data
   resultado db 'El resultado es:',10
   len_resultado equ $-resultado
    new_line db " ",10
    len_new_line equ $-new_line
   
section .bss
    multiplicacion resb 2 ;cuando no se nececita del enter
section .text
    global _start
_start:
    mov ax,3
    mov bx,2
    
    
    mul bx     ;eax=eax*ebx
    add ax,'0' ;Ajuste para que haga la suma en ascii

    ;add al,'0'
    ;add ah,'0'

    mov [multiplicacion], ax
    ;mov [multiplicacion + 1], ah

    mov eax, 4
    mov ebx, 1
    mov ecx, resultado
    mov edx, len_resultado
    int 80h

    mov eax, 4
    mov ebx, 1
    mov ecx, multiplicacion
    mov edx, 2
    int 80h

    mov eax, 4
    mov ebx, 1
    mov ecx, new_line
    mov edx, len_new_line
    int 80h

    mov eax, 1
    int 80h
