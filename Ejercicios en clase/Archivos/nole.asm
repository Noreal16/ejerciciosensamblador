%macro escribir 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
%macro imprimir 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
segment .data 
        msj1  db "Ingrese datos en el archivo ",10
        len_msj1 equ $-msj1
        new_line db '',10
        len_new_line equ $-new_line
        archivo1 db "/home/santy/Escritorio/ejerciciosensamblador/Ejercicios en clase/Archivos/datos.txt" ; id
        archivo2 db "/home/santy/Escritorio/ejerciciosensamblador/Ejercicios en clase/Archivos/resultado.txt" ; id
        
segment .bss
        texto resb 30
        idarchivo resb 1
        txt resb 30
        num resb 2 
segment .text  
        global _start
leer:        
        mov eax,3
        mov ebx,0 
        mov ecx, texto
        mov edx, 10
        int 80h
        ret 
_start:
        mov eax,5              ; servicio para crear archivos
        mov ebx,archivo1        ; direccion del archivo
        mov ecx, 0             ; modo de acceso
        mov edx,777h 
        int 80h

        test eax, eax   
        jz salir ;Se ejeucta cunaod existen errores en el archivo
        mov dword [idarchivo],eax
        escribir msj1,len_msj1
    
    
    
        mov eax, 3
        mov ebx, [idarchivo]
        mov ecx, txt
        mov edx, 15
        int 80h 
    ;***************cerrra el archivo*******************
        mov eax, 6
        mov ebx, [idarchivo]
        mov ecx, 0 
        mov edx, 0
        int 80h 


;********************Crear Archivo*************************
        mov eax, 8              ;establece(definimos) el servicio para crear archivos
        mov ebx, archivo2        ;direccion del archivo
        mov ecx, 1              ;definimos modo de acceso
        mov edx, 777h           ;se asigna los permisos al archivo
        int 80h

        test eax, eax           ;test hace una comparacion logica & entre dos operandos ...Para verificar si el archivo se creo de manera correcata o incorrecta
        jz salir                ; se ejecuta cuando tiene errores en el archivo

        mov dword [idarchivo], eax ;mandatorio para poder trabajar en archivos
        ;*******logica*************************************
        mov cx, 8 
ciclo:
        mov [txt], cx
        sub cx, '0'

        mov bx, cx
        mov cx, 9
        mov esi, txt
        mov edi, num 
        mov ecx, 20

        cld
        repe cmpsb
        je invertir
invertir:
        lodsb
        mov [edi],al
        dec edi
        loop invertir

        ;***************Escritura Archivo******************
        mov eax, 4
        mov ebx, [idarchivo]
        mov ecx, num  
        mov edx, 2
        int 0x80
salir:
        mov eax,1
        int 80h
    

