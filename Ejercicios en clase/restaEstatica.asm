;Resta de dos numero estaticos
section .data
   resultado db 'El resultado es:',10
   len_resultado equ $-resultado
    new_line db " ",10
    len_new_line equ $-new_line

section .bss
    resta resb 2 ;cuando no se nececita del enter
section .text
    global _start
_start:
    mov eax,5
    mov ebx,2
    sub eax, ebx ;eax=eax-ebx
    add eax, '0' ;Ajuste para que haga la suma en ascii

    mov [resta], eax

    mov eax, 4
    mov ebx, 1
    mov ecx, resultado
    mov edx, len_resultado
    int 80h

    mov eax, 4
    mov ebx, 1
    mov ecx, resta
    mov edx, 1
    int 80h

    mov eax, 4
    mov ebx, 1
    mov ecx, new_line
    mov edx, len_new_line
    int 80h
    
    mov eax, 1
    int 80h
