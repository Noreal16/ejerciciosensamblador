;Definicion de macro
%macro imprimir 2
        mov eax, 4
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro
;Definicion de macro
%macro leer 2
        mov eax, 3
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro

section .data
        msj db "ingrese valor ", 10
        len_msj equ $-msj

        msj1 db "Los impares son   : "
        len_msj1 equ $-msj1

        new_line db " " ,10
        len_new_line equ $-new_line
section .bss
        dato resb 2
        num resb 2
section .text
        global _start
_start:
        imprimir msj, len_msj

        leer dato, 2
        mov ax, [dato]
        sub ax, '0'
        push ax
operacion:
        add cx, '0'
        mov [num], cx
        sub cx, '0'
        
        push cx
        
        and cx, 1 ;compara si el numero es par
        jz presentar_cadena
        jmp recuperar

presentar_cadena:
        imprimir num, 2
        imprimir new_line, len_new_line
        jmp recuperar

recuperar:
        pop cx
        dec cx
        cmp cx, 0
        jz salir
        jmp operacion

        
salir:
        mov eax, 1
        int 80h