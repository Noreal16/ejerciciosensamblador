;Definicion de macro
%macro imprimir 2
        mov eax, 4
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro

section .data
        msj1 db "Los impares son   : "
        len_msj1 equ $-msj1

        new_line db " " ,10
        len_new_line equ $-new_line
section .bss

        num resb 2
section .text
        global _start
_start:
        mov cx, 1

operacion:
        add cx, '0'
        mov [num], cx
        sub cx, '0'

        push cx
        and cx, 1 ;compara si el numero es par
        jg presentar_cadena
        jmp recuperar

presentar_cadena:
        imprimir num, 2
        imprimir new_line, len_new_line
        jmp recuperar

recuperar:
        pop cx
        inc cx
        cmp cx, 10
        je salir
        jmp operacion

        
salir:
        mov eax, 1
        int 80h