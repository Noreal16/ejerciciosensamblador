;Definicion de macro
%macro imprimir 2
        mov eax, 4
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro

%macro lectura 2
        mov eax, 3
        mov ebx, 2
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro

section .data
        msj1 db "Ingrese Valor : "
        len_msj1 equ $-msj1
        new_line db " " ,10
        len_new_line equ $-new_line
section .bss

        num resb 2
section .text
        global _start
_start:

        imprimir msj1, len_msj1
        lectura num, 2

        mov cx, 4
        sub cx, '0'
        mov ax, 2
ciclo:
        push cx
        push ax
        mov bl, 0
        mov cl, 0
        mov dx, ax

incrementar_contador:
        inc bl 
        mov al, dl 
        div bl 
        cmp ah, 0
        je incrementar_primo
        jmp incrementar_contador
incrementar_primo:
        inc cl 
        cmp bl, dl 
        jge presentar
        jmp incrementar_contador

presentar:
        cmp dl, 4
        jg restar_contador
        jmp igual_contador
restar_contador:
        sub cl, 1
        cmp cl, 2
        jg no_primo
        je si_primo
        
no_primo:
        pop ax
        inc ax
        pop cx
        inc cx
        loop ciclo
        jmp salir
si_primo:
        add dl, '0'
        mov [num], dl 
        call presenta
        pop ax
        inc ax
        pop cx
        loop ciclo
        jmp salir
igual_contador:
        cmp cl, 2
        jg no_primo
        je si_primo
        
presenta:
        mov eax, 4
        mov ebx, 1
        mov ecx, num 
        mov edx, 2
        int 80h
        ret
salir:
        mov eax, 1
        int 80h

