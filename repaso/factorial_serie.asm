;Definicion de macro
%macro imprimir 2
        mov eax, 4
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro

section .data
        msj1 db "El factorial es  : "
        len_msj1 equ $-msj1

        msj db "Ingrese valor   : "
        len_msj equ $-msj

        new_line db 10," " 
        len_new_line equ $-new_line
section .bss

        num resb 2
        entrada resb 2
section .text
        global _start
_start:
        

        imprimir msj1, len_msj1
        imprimir new_line, len_new_line
        mov ax, 1
        mov cx, 3
        jmp proceso

proceso:
        
        push cx
        mov bx, cx
        mul bx

        push ax
        add ax, '0'
        mov [num], ax
        imprimir num, 2
        imprimir new_line, len_new_line
        
        pop cx
        pop ax
        dec cx
        cmp cx, 0
        jne proceso

        
salir:
        mov eax, 1
        int 80h

