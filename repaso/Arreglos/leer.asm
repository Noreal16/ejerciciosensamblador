%macro escribir 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
%macro imprimir 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
segment .data 
        msj1  db "Ingrese datos en el archivo ",10
        len_msj1 equ $-msj1
        msj2  db  "son iguales ",10
        len_msj2 equ $-msj2
        msj3  db "No son iguales ",10
        len_msj3 equ $-msj3
        new_line db '',10
        len_new_line equ $-new_line
        archivo db "/home/santy/Escritorio/repaso/Arreglos/archivs.txt" ; id
        
segment .bss
    texto resb 30
    idarchivo resb 1
    txt resb 30 
segment .text  
        global _start
leer:        
    mov eax,3
    mov ebx,0 
    mov ecx, texto
    mov edx, 10
    int 80h
    ret 
_start:


    ;*************Abrir el archivo******************
    mov eax,5              ; servicio para crear archivos
    mov ebx,archivo        ; direccion del archivo
    mov ecx, 0             ; modo de acceso
                                ;O-Rdonly 0 : el archivo se abre sólo para leer 
                                ;O-wRONLY 1; EL ARCHIVO SE ABRE PARA ESCRITURA
                                ;O-RDWR  2: El archivo se abre para escritura y lectura 
                                ;O-CREATE 256: Crea el archivo en caso no existe
                                ;O-APPEND  2000H: El archivo se abre solo para escritura al final
    mov edx,777h 
    int 80h

    test eax, eax   
    jz salir ;Se ejeucta cunaod existen errores en el archivo
    mov dword [idarchivo],eax
    escribir msj1,len_msj1
    
    call leer
    ;****************Leer el archivo*******************
    mov eax, 3
    mov ebx, [idarchivo]
    mov ecx, txt
    mov edx, 15
    int 80h 

    
    ;***************cerrra el archivo*******************
    mov eax, 6
    mov ebx, [idarchivo]
    mov ecx, 0 
    mov edx, 0
    int 80h 
contar:
    mov esi, texto
    mov edi, txt
    mov ecx, 20

    cld
    repe cmpsb
    jecxz impri 
    jmp no_iguales

impri: 
    imprimir msj2,len_msj2
    imprimir texto,2
    imprimir new_line,len_new_line
    imprimir txt,2
    jmp salir
no_iguales:
    imprimir msj3,len_msj3
    jmp salir

salir:
    mov eax,1
    int 80h
    

