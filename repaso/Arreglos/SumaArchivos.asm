%macro escribir 2 
        mov eax, 4
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro

section .data
        msj1 db "Ingrese datos en el archivo",10
        len1 equ $ -msj1

        archivo db "/home/santy/Escritorio/repaso/Arreglos/archivo2.txt"
section .bss
        texto resb 30
        idarchivo resd 1
        num resb 2
        resp resb 2
section .text
        global _start
leer:
        mov eax, 3
        mov ebx, 0
        mov ecx, num 
        mov edx, 2
        int 80h
        ret 

_start:
        mov eax, 8              ;establece(definimos) el servicio para crear archivos
        mov ebx, archivo        ;direccion del archivo
        mov ecx, 1              ;definimos modo de acceso
                                    ;O-RDONY  0: EL archivo se abre solo para leer
                                    ;O-WRONLy 1: El archivo se abre para escritura
                                    ;O-RDWR   2: El archivo se abre para escritura y lectura
                                    ;O-CREATE 256: Crear el archivo en caso que no exista
                                    ;O-APPEND 2000h: El archivo se abre solo para escritura al final
        mov edx, 777h           ;se asigna los permisos al archivo
        int 80h

        test eax, eax           ;test hace una comparacion logica & entre dos operandos ...Para verificar si el archivo se creo de manera correcata o incorrecta
        jz salir                ; se ejecuta cuando tiene errores en el archivo

        mov dword [idarchivo], eax ;mandatorio para poder trabajar en archivos
        escribir msj1, len1
        
        call leer
        mov ax, 0
        mov cx, [num]
        sub cx, '0'

ciclo:
        add ax, cx
        loop ciclo
        add al, '0'
        mov [resp], al

        ;call leer
        

        ;***********************Escritura del Archivo****************************
        mov eax, 4
        mov ebx, [idarchivo]
        mov ecx, resp
        mov edx, 2
        int 0x80
salir:
    mov eax,1
    int 80h
