%macro escribir 2 
        mov eax, 4
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro

%macro lectura 2
        mov eax, 3
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro
section .data
        msj1 db "El factorial es  :",10
        len1 equ $ -msj1
        
        msj2 db "El Ingrese segundo valor",10
        len2 equ $ -msj2

        archivo db "/home/santy/Escritorio/repaso/Arreglos/archivo1.txt"
        
section .bss
        texto resb 30
        idarchivo resd 1
        num resb 2
section .text 
        global _start

_start:
        mov eax, 8              ;establece(definimos) el servicio para crear archivos
        mov ebx, archivo        ;direccion del archivo
        mov ecx, 1              ;definimos modo de acceso
        mov edx, 777h           ;se asigna los permisos al archivo
        int 80h

        test eax, eax           ;test hace una comparacion logica & entre dos operandos ...Para verificar si el archivo se creo de manera correcata o incorrecta
        jz salir                ; se ejecuta cuando tiene errores en el archivo

        mov dword [idarchivo], eax ;mandatorio para poder trabajar en archivos
        ;**************************Logica del programa***************************
        mov ax, 1
        mov cx, 1
        jmp proceso

        proceso:
        push cx
        mov bx, cx
        mul bx

        push ax
        add ax, '0'
        mov [num], ax
        pop cx
        pop ax
        inc cx
        cmp cx, 3
        jng proceso
        ;***********************Escritura del Archivo****************************
        mov eax, 4
        mov ebx, [idarchivo]
        mov ecx, num
        mov edx, 2
        int 0x80

salir:
        mov eax, 1
        int 0x80