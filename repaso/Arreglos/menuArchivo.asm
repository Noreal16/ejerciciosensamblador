;Ejercicio calculadora saltos y cmp
;Alex Nole

;macro para la presentar
%macro escritura 2
        mov eax, 4
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro
;macro para la lectura por teclado 
%macro lectura 2
        mov eax, 3
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro

%macro escribir 2 
        mov eax, 4
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro

section .data
	    msg1		db		10,'-Menu de Opciones-',10,0
	    lmsg1		equ		$ - msg1

        msg2		db		10,'-1.- Escribir archivo-',10,0
	    lmsg2		equ		$ - msg2

        msg3		db		10,'-2.- Leer Archivo-',10,0
	    lmsg3		equ		$ - msg3

        msg4		db		10,'-3.- Salir-',10,0
	    lmsg4		equ		$ - msg4

	    msg10		db		10,'-Opcion Invalida-',10,0
	    lmsg10		equ		$ - msg10
	
	    msg11		db		10,'-Seleccione opcion-',10,0
	    lmsg11		equ		$ - msg11

        msj1 db "Ingrese datos en el archivo ",10
        len1 equ $ -msj1

        msj2 db "EL archivo contiene ",10
        len2 equ $ -msj2

	    nlinea		db		10,10,0
	    lnlinea		equ		$ - nlinea 

        archivo db "/home/santy/Escritorio/repaso/Arreglos/archivs.txt"
section .bss
  	    opcions:    resb    2
        texto resb 30
        idarchivo resd 1

section .text
 	    global _start
leer:
        mov eax, 3
        mov ebx, 0
        mov ecx, texto
        mov edx, 10
        int 80h
        ret 
_start:
menu:
        escritura msg1, lmsg1
        escritura msg2, lmsg2
        escritura msg3, lmsg3
        escritura msg4, lmsg4
        escritura msg11, lmsg11
        
        lectura opcions, 2
        
        mov ah, [opcions]
        sub ah, '0'

        cmp ah, 1
        je escribir_archivo

        cmp ah, 2
        je leer_archivo

        cmp ah, 3
        je  salir

        escritura msg10, lmsg10
        jmp salir    
escribir_archivo:
                mov eax, 8              ;establece(definimos) el servicio para crear archivos
        mov ebx, archivo        ;direccion del archivo
        mov ecx, 1              ;definimos modo de acceso
                                    ;O-RDONY  0: EL archivo se abre solo para leer
                                    ;O-WRONLy 1: El archivo se abre para escritura
                                    ;O-RDWR   2: El archivo se abre para escritura y lectura
                                    ;O-CREATE 256: Crear el archivo en caso que no exista
                                    ;O-APPEND 2000h: El archivo se abre solo para escritura al final
        mov edx, 777h           ;se asigna los permisos al archivo
        int 80h

        test eax, eax           ;test hace una comparacion logica & entre dos operandos ...Para verificar si el archivo se creo de manera correcata o incorrecta
        jz salir                ; se ejecuta cuando tiene errores en el archivo

        mov dword [idarchivo], eax ;mandatorio para poder trabajar en archivos
        escribir msj1, len1
        
        call leer

        ;***********************Escritura del Archivo****************************
        mov eax, 4
        mov ebx, [idarchivo]
        mov ecx, texto
        mov edx, 20
        int 0x80
        jmp menu
leer_archivo:
                ;*****************Abrir el archivo****************************************
        mov eax, 5              ;establece(definimos) el servicio para crear archivos
                                ;permite hacer open en el archivo
        mov ebx, archivo        ;direccion del archivo
        mov ecx, 0              ;definimos modo de acceso
        mov edx, 777h           ;se asigna los permisos al archivo
        int 80h

        test eax, eax           ;test hace una comparacion logica & entre dos operandos ...Para verificar si el archivo se creo de manera correcata o incorrecta
        jz salir                ; se ejecuta cuando tiene errores en el archivo

        mov dword [idarchivo], eax ;mandatorio para poder trabajar en archivos
        escribir msj2, len2
        ;**********************leer el archivo**********************************
        mov eax, 3
        mov ebx, [idarchivo]
        mov edx, texto
        mov edx, 15
        int 80h

        escribir texto, 15
        ;***********************cerrar el Archivo****************************
        mov eax, 6              ;clode
        mov ebx, [idarchivo]
        mov ecx, 0
        mov edx, 0
        int 0x80
        jmp menu


salir:
        mov eax, 1
        int 80h