%macro escribir 2 
        mov eax, 4
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro

%macro lectura 2
        mov eax, 3
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro
section .data
        msj1 db "Ingrese primer valor",10
        len1 equ $ -msj1
        
        msj2 db "El Ingrese segundo valor",10
        len2 equ $ -msj2

        archivo db "/home/santy/Escritorio/repaso/Arreglos/archivosss.txt"
        
section .bss
        texto resb 30
        idarchivo resd 1
        numero resb 2
        numer2 resb 2
        resul resb 2
section .text 
        global _start
leer1:
        mov eax, 3
        mov ebx, 0
        mov ecx, numero 
        mov edx, 2
        int 80h
        ret 
leer2:
        mov eax, 3
        mov ebx, 0
        mov ecx, numer2 
        mov edx, 2
        int 80h
        ret 
_start:
        mov eax, 8              ;establece(definimos) el servicio para crear archivos
        mov ebx, archivo        ;direccion del archivo
        mov ecx, 1              ;definimos modo de acceso
        mov edx, 777h           ;se asigna los permisos al archivo
        int 80h

        test eax, eax           ;test hace una comparacion logica & entre dos operandos ...Para verificar si el archivo se creo de manera correcata o incorrecta
        jz salir                ; se ejecuta cuando tiene errores en el archivo

        mov dword [idarchivo], eax ;mandatorio para poder trabajar en archivos

        escribir msj1, len1
        call leer1

        escribir msj2, len2
        call leer2

        
        mov ax,[numero]
        mov bx,[numer2]
        sub ax, '0'
        sub bx, '0'

        add ax, bx
        add ax, '0'
        mov [resul], ax
        ;escribir resul, 2

        ;***********************Escritura del Archivo****************************
        mov eax, 4
        mov ebx, [idarchivo]
        mov ecx, resul
        mov edx, 2
        int 0x80

salir:
        mov eax, 1
        int 0x80