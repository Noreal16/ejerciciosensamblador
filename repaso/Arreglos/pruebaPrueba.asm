%macro imprimir 2
    mov eax, 4
    mov ebx, 1
    mov ecx,%1
    mov edx,%2
    int 80h
%endmacro

%macro leer 2
    mov eax,3
    mov ebx,0
    mov ecx,%1
    mov edx,%2
    int 80h
%endmacro

%macro write 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro

section .data
    pathNum1 db '/home/cesar/Documentos/Ensamblador/NASM/1.txt',0
    pathNum2 db '/home/cesar/Documentos/Ensamblador/NASM/2.txt',0
    pathMore db '/home/cesar/Documentos/Ensamblador/NASM/suma.txt',0

    msj0 db '-------------------------------------------'
    len_msj0 equ $-msj0
    msj1 db 10,'Menu de Opciones'
    len_msj1 equ $-msj1
    msj2 db 10,'1. Escribir archivo para el primer numero'
    len_msj2 equ $-msj2
    msj3 db 10,'2. Escribir archivo para el segundo numero'
    len_msj3 equ $-msj3
    msj4 db 10,'3. Presentar resultado'
    len_msj4 equ $-msj4
    msj5 db 10,'4. Salir',10
    len_msj5 equ $-msj5
    msj6 db 10,'Introdusca una opcion: '
    len_msj6 equ $-msj6
    msj7 db 10,'Opcion no valida',10,0
	len_msj7 equ $-msj7

    mensaje db 'Ingrese el valor: '
    len_mensaje equ $-mensaje
    resultado db 'El resultado es: '
    len_resultado equ $-resultado

    mjsError db 'Error en el archivo',10
    lenError equ $-mjsError

    msjMore db ' + '
    msjEqual db ' = '
    
    newLine db '',10

section .bss
    opcion resb 2
    
    num1 resb 1
    num2 resb 1

    idNum1 resb 1
    idNum2 resb 1

    suma resb 2
    idFile resb 1
section .text
    global _start

leer1:
	mov eax, 3
	mov ebx, 0
	mov ecx, num1
	mov edx, 256
	int 80h
	ret
leer2:
	mov eax, 3
	mov ebx, 0
	mov ecx, num2
	mov edx, 256
	int 80h
	ret

_start:
    
menu:
    imprimir msj0, len_msj0
    imprimir msj1, len_msj1
    imprimir msj2, len_msj2
    imprimir msj3, len_msj3
    imprimir msj4, len_msj4
    imprimir msj5, len_msj5
    imprimir msj0, len_msj0
    imprimir msj6, len_msj6
    leer opcion, 2

    mov ah, [opcion]
    sub ah, '0'

    cmp ah, 1
    je escribir_archivo1

    cmp ah, 2
    je escribir_archivo2

    cmp ah, 3
    je  readFileNum1

    cmp ah, 4
    je  salir

    imprimir msj7,len_msj7
    jmp salir  

escribir_archivo1:
    mov eax, 8 		;servicio para crear archivos, trabajar con archivos
	mov ebx, pathNum1; dirección del archivo
	mov ecx, 1		; MODO DE ACCESO
					; O-RDONLY 0: El archivo se abre sólo para leer
					; O-WRONLY 1: El archivo se abre para escritura
					; O-RDWR 2: El archivo se abre para escritura y lectura
					; O-CREATE 256: Crea el archivo en caso que no exist
					; O-APPEND 2000h: El archivo se abro solo par escritura al final
	mov edx, 777h
	int 80h
	
	test eax, eax	; instrucción de comparación realiza la operación lógica “Y” de dos operandos, 
				    ; pero NO afecta a ninguno de ellos, SÓLO afecta al registro de estado. Admite 
				    ; todos los tipos de direccionamiento excepto los dos operandos en memoria
					; TEST reg, reg
					; TEST reg, mem
					; TEST mem, reg
					; TEST reg, inmediato
					; TEST mem, inmediato 
	
	jz salir		; se ejecuta cuando existen errores en el archivo
	
	mov dword [idNum1], eax
	
    imprimir mensaje, len_mensaje
	
	call leer1
	
	; ************************ escritura en el archivo *****************************
	mov eax, 4
	mov ebx, [idNum1]
	mov ecx, num1
	mov edx, 256
	int 0x80

    jmp menu

escribir_archivo2:
    mov eax, 8 		;servicio para crear archivos, trabajar con archivos
	mov ebx, pathNum2; dirección del archivo
	mov ecx, 1		; MODO DE ACCESO
					; O-RDONLY 0: El archivo se abre sólo para leer
					; O-WRONLY 1: El archivo se abre para escritura
					; O-RDWR 2: El archivo se abre para escritura y lectura
					; O-CREATE 256: Crea el archivo en caso que no exist
					; O-APPEND 2000h: El archivo se abro solo par escritura al final
	mov edx, 777h
	int 80h
	
	test eax, eax	; instrucción de comparación realiza la operación lógica “Y” de dos operandos, 
				    ; pero NO afecta a ninguno de ellos, SÓLO afecta al registro de estado. Admite 
				    ; todos los tipos de direccionamiento excepto los dos operandos en memoria
					; TEST reg, reg
					; TEST reg, mem
					; TEST mem, reg
					; TEST reg, inmediato
					; TEST mem, inmediato 
	
	jz salir		; se ejecuta cuando existen errores en el archivo
	
	mov dword [idNum1], eax
	
    imprimir mensaje, len_mensaje
	
	call leer2
	
	; ************************ escritura en el archivo *****************************
	mov eax, 4
	mov ebx, [idNum1]
	mov ecx, num2
	mov edx, 256
	int 0x80

    jmp menu

readFileNum1:
    mov eax, 5 
    mov ebx, pathNum1
    mov ecx, 0
    mov edx, 0
    int 80h

    test eax, eax
    jz errorFile
    mov dword[idNum1], eax
    
    mov eax, 3
    mov ebx, [idNum1]
    mov ecx, num1
    mov edx, 1 ;len
    int 80h

    write newLine, 1
    write resultado, len_resultado
    write num1, 1
    ; close file 1
    mov eax, 6
    mov ebx, [idNum1]
    mov ecx, 0
    mov edx, 0x1FF
    int 80h

    write msjMore,3

readFileNum2:
    mov eax, 5
    mov ebx, pathNum2
    mov ecx, 0
    mov edx, 0
    int 80h

    test eax, eax
    jz errorFile
    mov dword[idNum2], eax
    mov eax, 3
    mov ebx, [idNum2]
    mov ecx, num2
    mov edx, 1 ;len
    int 80h

    write num2, 1
    ; close file 2
    mov eax, 6
    mov ebx, [idNum2]
    mov ecx, 0
    mov edx, 0x1FF
    int 80h

    write msjEqual,3

sumaNumero:
    mov eax, [num1]
    mov ebx, [num2]
    sub eax, '0'
    sub ebx, '0'
    add eax, ebx
    add eax, '0'
    mov [suma], eax

    write suma, 1
    
    write newLine,1
    jmp menu  

writeFileMore:
    ;***access the file***
    mov eax, 8
    mov ebx, pathMore
    mov edx, 0x1FF
    int 80h

    test eax,eax
    jz errorFile

    mov dword [idFile], eax

    ;asigna el valor de la suma hacia el archivo
    mov eax,4
    mov ebx,[idFile]
    mov ecx,suma
    mov edx,1
    int 80h
    
    ;****close file***
    mov eax,6
    mov ebx,[idFile]
    mov ecx,0
    mov edx,0x1FF
    int 80h

    jmp salir

errorFile:
    write mjsError, lenError

salir:
    mov eax, 1
    int 80h