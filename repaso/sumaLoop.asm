;Definicion de macro
%macro imprimir 2
        mov eax, 4
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro

section .data
        msj1 db "la suma del ciclo es : "
        len_msj1 equ $-msj1
        new_line db " " ,10
        len_new_line equ $-new_line
section .bss

        num resb 2
section .text
        global _start
_start:
        mov ax, 0
        mov cx, 3

loop_for: 
        add ax, cx
        loop loop_for

        add ax, '0'
        mov [num], ax

        imprimir msj1, len_msj1
        imprimir num, 2
        imprimir new_line, len_new_line 

salir:
        mov eax, 1
        int 80h

