;Definicion de macro
%macro imprimir 2
        mov eax, 4
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro

section .data
        msj1 db "El factorial es  : "
        len_msj1 equ $-msj1
        new_line db " " ,10
        len_new_line equ $-new_line
section .bss

        num resb 2
section .text
        global _start
_start:
        mov ax, 1
        mov cx, 3
loop_for:
        push cx
        mov bx, cx
        mul bx
        
        push ax
        add ax, '0'
        mov [num], ax
        
        int 80h
        pop ax
        pop cx
        loop loop_for

        imprimir msj1, len_msj1
        imprimir num, 2
        imprimir new_line, len_new_lines
salir:
        mov eax, 1
        int 80h
