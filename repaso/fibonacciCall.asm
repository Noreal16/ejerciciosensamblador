;Definicion de macro
%macro imprimir 2
        mov eax, 4
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro

section .data
        msj1 db "El fibonacci es  : "
        len_msj1 equ $-msj1
        new_line db " " ,10
        len_new_line equ $-new_line
section .bss

        num resb 2
section .text
        global _start
_start:
        imprimir msj1, len_msj1
        imprimir new_line, new_line
        mov ax, 0
        mov bx, 1
        mov cx, 0
        mov dx, 0
        jmp proceso
        
proceso:
        mov ax, bx
        mov bx, cx

        mov dx, ax
        add dx, bx

        mov cx, dx
        push cx

        cmp cx, 4
        jng proceso
        jmp recuperar_valor

recuperar_valor:
        mov ax, 0
        pop cx
        cmp cx, 0
        jg imprime ; imprime de menor a mayor
        jmp salir

imprime:
        add cx, '0'
        mov [num], cx
        call presenta
        imprimir new_line, len_new_line
        jmp recuperar_valor
        
presenta:
        mov eax, 4
        mov ebx, 1
        mov ecx, num 
        mov edx, 2
        int 80h
        ret
        
salir:
        mov eax, 1
        int 80h
