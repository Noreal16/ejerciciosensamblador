;Wilson Lizandro Valverde Jadan
;Examen 3 Unidad
;Divisiñon sin instrucción div
%macro imprimir 2 
        mov rax, 4
        mov rbx, 1
        mov rcx, %1
        mov rdx, %2
        int 80h
%endmacro
%macro registrarvariables 2 
        mov rax, 3
        mov rbx, 0
        mov rcx, %1
        mov rdx, %2
        int 80h
%endmacro
section .data
    saludo_msj db  10,"Algoritmo de División sin instrucción div:",10
    len_saludo equ $ - saludo_msj
    dividendo_msj db  10,"Ingrese el divisor:"
    len_dividendo equ $ - dividendo_msj

    divisor_msj db  10,"Ingrese el dividendo:"
    len_divisor equ $ - divisor_msj

    resultado1_msj db 10, "El cociente es:"
    len_resultado1 equ $ - resultado1_msj

    resultado2_msj db 10, "El residuo es:"
    len_resultado2 equ $ - resultado2_msj
    fin_msj db  10,"Fin de la ejecución ",10
    len_fin equ $ - fin_msj

section .bss
    divisor resb 1
    dividendo resb 1
    cociente resb 1
    residuo resb 1
section .text
        global _start 
_start:
    imprimir saludo_msj, len_saludo
    imprimir dividendo_msj, len_dividendo
    registrarvariables divisor, 2
    imprimir divisor_msj, len_divisor
    registrarvariables dividendo,2
    mov al, [divisor]
    sub al, '0'
    mov bl, [dividendo]
    sub bl, '0'
    mov rcx, 0
operaccion: 
    sub al, bl
    inc rcx
    cmp al,bl
    jg operaccion
    je operaccion
    call presentacion
presentacion: 
	add al,'0'
	add rcx,'0'
	mov [cociente], rcx
	mov [residuo], al
    call mensajes

mensajes:
    imprimir resultado1_msj,len_resultado1
    imprimir cociente,1
    imprimir resultado2_msj, len_resultado2
    imprimir residuo,1
    imprimir fin_msj,len_fin
    ret
salir: 
        mov eax, 1
        int 80h