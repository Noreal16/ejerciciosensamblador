section .data
    ;datos db "Alex Santiago",10,"Nole Reyes",10,"Ensamblador",10,"Masculino",10
    ;len EQU $-datos
    nombres db "Alex Santiago",10
    len_nombres EQU $-nombres

    apellidos db "Nole Reyes",10
    len_apellidos EQU $-apellidos
    
    materia db "Ensamblador",10
    len_materia EQU $-materia 
    
    genero db "M",10
    len_genero EQU $-genero
section .text
 global _start
  _start:

    mov eax, 4 ; Tipo de subrutina => operaacion de escritura
	mov ebx, 1 ; estanadar del tipo de salidas=>for keywork
	mov ecx, nombres ;el registro ecx se almacena la ref a imprimir 
	mov edx, len_nombres ; el registro edx se almacena en la referencia a imprimi por # de caracteres
	int 80h ; interrupcion de software para el so linux

    mov eax, 4 ; Tipo de subrutina => operaacion de escritura
	mov ebx, 1 ; estanadar del tipo de salidas=>for keywork
	mov ecx, apellidos ;el registro ecx se almacena la ref a imprimir 
	mov edx, len_apellidos ; el registro edx se almacena en la referencia a imprimi por # de caracteres
	int 80h ; interrupcion de software para el so linux

    mov eax, 4 ; Tipo de subrutina => operaacion de escritura
	mov ebx, 1 ; estanadar del tipo de salidas=>for keywork
	mov ecx, materia ;el registro ecx se almacena la ref a imprimir 
	mov edx, len_materia ; el registro edx se almacena en la referencia a imprimi por # de caracteres
	int 80h ; interrupcion de software para el so linux

    mov eax, 4 ; Tipo de subrutina => operaacion de escritura
	mov ebx, 1 ; estanadar del tipo de salidas=>for keywork
	mov ecx, genero ;el registro ecx se almacena la ref a imprimir 
	mov edx, len_genero ; el registro edx se almacena en la referencia a imprimi por # de caracteres
	int 80h ; interrupcion de software para el so linux
	
	mov eax, 1	 ;salida del oprograma, system _exit
	mov ebx, 0       ;ses ek retorno 0(es como 200 en la web) ok
	int 80h 